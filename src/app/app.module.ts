import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AuthComponent } from './auth/auth.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { UpsertComponent } from './upsert/upsert.component';
import { DetailComponent } from './detail/detail.component';
import { UserService } from './services/user.service';
import { PostComponent } from './post/post.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserPostsComponent } from './user-posts/user-posts.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { PostEditableComponent } from './post-editable/post-editable.component';
import { EditPostComponent } from './edit-post/edit-post.component';
import { ChangePassComponent } from './change-pass/change-pass.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    AuthComponent,
    RegistrationComponent,
    UpsertComponent,
    DetailComponent,
    PostComponent,
    UserPostsComponent,
    PostEditableComponent,
    EditPostComponent,
    ChangePassComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    Ng2SearchPipeModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
