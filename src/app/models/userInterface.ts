export interface UserInterface {
    userId: number;
    userName: string;
    email:  string;
    passwordHashed: string;
    phoneNumber: string;
    // posts?: Array<any>;
    // comments?: Array<any>;

}