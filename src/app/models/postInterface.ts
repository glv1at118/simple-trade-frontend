import {UserInterface} from './userInterface';

export interface PostInterface{

    postId: number;
    availability: boolean;
    postTitle: string;
    postDescription: string;
    dateCreated: string;
    numberOfVisits: number;
    imageURL: string;
    price: number;
    userHolder: UserInterface;
    // commentList: Comment;

}
