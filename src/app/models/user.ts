export class User {
    constructor(public userName: string, public email: string, 
        public passwordHashed: string, public phoneNumber: string, public posts?: Array<any>, 
        public comments?: Array<any>, public userId?: number) {
        }
}
