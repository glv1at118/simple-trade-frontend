import { Component, OnInit } from '@angular/core';
import { Post } from '../models/upsert';
import { User } from '../models/user';
import { UpsertService } from '../services/upsert.service';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';

@Component({
    selector: 'app-upsert',
    templateUrl: './upsert.component.html',
    styleUrls: ['./upsert.component.css']
})
export class UpsertComponent implements OnInit {

    userReturned: boolean = false;
    postList: Post[];
    postTitle: string = "";
    price: number = 0;
    postDescription: string;
    user: User;
    fileUrl: string = "";
    fileExist: boolean = false;

    constructor(private postServ: UpsertService, private httpClient: HttpClient) { }

    ngOnInit(): void {

    }

    ngDoCheck(): void {
        if (this.fileUrl) {
            this.fileExist = true;
        } else {
            this.fileExist = false;
        }
    }

    // when submitting the post, submits the post with the already existing fileUrl (image url on s3)
    public submitPost() {
        if (this.postTitle == "" || this.postDescription == "" || this.price == 0) {
            alert("Please fill in all fields first before submitting a product post.");
            return;
        }

        this.user = JSON.parse(localStorage.getItem("user"))
        let stringPost = {
            "availability": "true",
            "postTitle": this.postTitle,
            "postDescription": this.postDescription,
            "dateCreated": "1620778459",
            "numberOfVisits": "0",
            "price": this.price,
            "imageURL": this.fileUrl,
            "userHolder": {
                "userId": this.user.userId,
                "userName": this.user.userName,
                "email": this.user.email,
                "passwordHashed": this.user.passwordHashed,
                "phoneNumber": this.user.phoneNumber
            }
        };
        console.log(stringPost);
        this.postServ.postPost(JSON.stringify(stringPost)).subscribe(
            response => {

            }
        )
    }

    // when a file is chosen, this method is triggered, then it sends the file to spring server.
    chooseImage(event): void {
        let file: File = event.target.files[0];
        let data: FormData = new FormData();
        data.append("file", file);

        let newRequest = new HttpRequest("POST", "http://ec2-54-153-124-53.us-west-1.compute.amazonaws.com:9090/api/upload", data, {
            reportProgress: false,
            responseType: "text"
        });
        this.httpClient.request(newRequest).subscribe(res => {
            this.fileUrl = res["body"];
        }, err => {
        });
    }

}
