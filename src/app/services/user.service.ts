import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private urlBase = "http://ec2-54-153-124-53.us-west-1.compute.amazonaws.com:9090/api/user/auth";
  constructor(private httpClient: HttpClient) { 
  }

  public getAuthUser(username: string, password: string): Observable<User> {
    const httpHead = {
      headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
      })
    };
    
   return this.httpClient.post<User>(this.urlBase, {
        "userName": username,
        "passwordHashed": password
    }, httpHead);
  }
}
