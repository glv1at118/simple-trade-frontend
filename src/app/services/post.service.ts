import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PostInterface } from '../models/postInterface';


@Injectable({
  providedIn: 'root'
})
export class PostService {

  private urlBase = "http://ec2-54-153-124-53.us-west-1.compute.amazonaws.com:9090/api/post/";

  constructor(private httpClient: HttpClient) { }


  public getPost(postId): Observable<PostInterface>{
    return this.httpClient.get<PostInterface>(this.urlBase + postId)
  }

  public deletePost(postId): Observable<any>{
    const httpHead = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }),
      responseType: 'text' as 'json'
    };
    return this.httpClient.delete<any>(this.urlBase+postId, httpHead);
  }



}


