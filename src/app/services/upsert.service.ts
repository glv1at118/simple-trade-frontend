import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UpsertService {


    private urlBase = "http://ec2-54-153-124-53.us-west-1.compute.amazonaws.com:9090/api/post"
    constructor(private httpCli: HttpClient) { }


    public postPost(post): Observable<any> {
        const httpHead = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'


            }),
            responseType: 'text' as "json"

        };

        return this.httpCli.post<any>(this.urlBase, post, httpHead);
    }

}
