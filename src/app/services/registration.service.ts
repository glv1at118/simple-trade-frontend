import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError  } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  private urlBase = "http://ec2-54-153-124-53.us-west-1.compute.amazonaws.com:9090/api/user";
  constructor(private httpCli: HttpClient) { }

  public postUser(user): Observable<string>{
    const httpHead={
      headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin':'*'
      }),
      responseType: 'text' as "json"
    };
    return this.httpCli.post<string>(this.urlBase, user, httpHead).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Please fill in all fields`;
    } else {
      // server-side error
      errorMessage = `Please fill in all fields to register`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
