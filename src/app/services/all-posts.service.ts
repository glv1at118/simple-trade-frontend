import { HttpClient, HttpParams } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {PostInterface} from '../models/postInterface';


@Injectable({
  providedIn: 'root'
})
export class AllPostsService {

  private urlBase = "http://ec2-54-153-124-53.us-west-1.compute.amazonaws.com:9090/api/post/all";
  constructor(private httpClient: HttpClient) { }

  public getAllPosts(): Observable<Array<PostInterface>>{
    return this.httpClient.get<Array<PostInterface>>(this.urlBase);
  }

}
