import { Component, DoCheck, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { UserTools } from '../UserTools';
import { Router } from '@angular/router';
import { User } from '../models/user';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit, DoCheck {

    title: string;
    description: string;
    price: number;
    email: string;
    phone: string;
    numberOfVisits: number;
    dateCreated: string;
    availability: boolean;
    postId: number;
    comments: any;
    imageUrl: string;
    textcontent: string;
    getCertainPostUrl: string = 'http://ec2-54-153-124-53.us-west-1.compute.amazonaws.com:9090/api/post/';
    incrementUrl: string = 'http://ec2-54-153-124-53.us-west-1.compute.amazonaws.com:9090/api/post/click/';
    getCommentsForAPostUrl: string = "http://ec2-54-153-124-53.us-west-1.compute.amazonaws.com:9090/api/post/comments/";
    createCertainCommentUrl: string = "http://ec2-54-153-124-53.us-west-1.compute.amazonaws.com:9090/api/comment";
    displayNoCommentText: boolean = true;

    constructor(private httpClient: HttpClient, private route: ActivatedRoute, private router: Router) { }

    ngOnInit(): void {
        let tool: UserTools = new UserTools();
        if (!tool.isUserLoggedIn()) {
            this.router.navigate(['/login']);
        } else {
            this.loadContent();
        }
    }

    ngDoCheck(): void {
        if(typeof this.comments==="undefined" || this.comments.length===0){
            this.displayNoCommentText = true;
        } else {
            this.displayNoCommentText = false;
        }
    }

    loadContent(): void {
        let currPostId = this.route.snapshot.paramMap.get("id");

        this.httpClient.get<string>(this.getCertainPostUrl + currPostId).subscribe(response => {
            

            this.title = response["postTitle"];
            this.description = response["postDescription"];
            this.price = response["price"];
            this.numberOfVisits = response["numberOfVisits"];
            this.dateCreated = response["dateCreated"];

            this.dateCreated = this.dateCreated.split("T")[0] + " / " + this.dateCreated.split("T")[1].split(".")[0];

            this.availability = response["availability"];
            this.email = response["userHolder"]["email"];
            this.phone = response["userHolder"]["phoneNumber"];
            this.postId = response["postId"];
            this.imageUrl = response["imageURL"];

            this.httpClient.get<string>(this.getCommentsForAPostUrl + this.postId).subscribe(msg => {
              
                this.comments = msg;
                for (let i = 0; i < this.comments.length; i++) {
                    let arr = this.comments[i].dateCreated.split("T");
                    this.comments[i].dateCreated = arr[0] + " / " + arr[1].split(".")[0];
                }
            });

            this.httpClient.get<string>(this.incrementUrl + this.postId).subscribe();
        });
    }

    createComment(): void {
        let currentUserInfo = JSON.parse(localStorage.getItem("user"));

        this.httpClient.post<string>(this.createCertainCommentUrl, {
            "userHolder": {
                "userId": currentUserInfo.userId,
                "userName": currentUserInfo.userName,
                "email": currentUserInfo.email,
                "passwordHashed": currentUserInfo.passwordHashed,
                "phoneNumber": currentUserInfo.phoneNumber
            },
            "postHolder": {
                "postId": this.route.snapshot.paramMap.get("id"),
                "availability": "true",
                "postTitle": "default",
                "postDescription": "default",
                "dateCreated": "0",
                "numberOfVisits": "0",
                "imageURL": "default",
                "userHolder": {
                    "userId": "0",
                    "userName": "default",
                    "email": "default",
                    "passwordHashed": "default",
                    "phoneNumber": "default"
                }
            },
            "text": this.textcontent,
            "dateCreated": "0"
        }).subscribe(response => {
            // if response has error, logic in error will be triggered instead
        }, error => {
            this.loadContent();
            this.textcontent = "";
        });
    }

    public sendEmail(): void {
        var postOwner =new User("", this.email,"", "");
        let currentUser = JSON.parse(localStorage.getItem("user"));
        let currPostId = this.route.snapshot.paramMap.get("id");
        const httpHead = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            }),
            responseType: 'text' as "json"
        };
        this.httpClient.post<string>(this.getCertainPostUrl + currPostId, currentUser, httpHead).subscribe(response => {
        });
        this.httpClient.post<string>(this.getCertainPostUrl + currPostId, postOwner, httpHead).subscribe(response => {
            window.alert("Email sent!");
        });
        
    }
}



