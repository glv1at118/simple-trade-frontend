import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PostInterface } from '../models/postInterface';
import {UserPostsService} from '../services/user-posts.service';
import { UserTools } from '../UserTools';
import { Router } from '@angular/router';
import { User } from '../models/user';

@Component({
  selector: 'app-user-posts',
  templateUrl: './user-posts.component.html',
  styleUrls: ['./user-posts.component.css']
})
export class UserPostsComponent implements OnInit {

  
  postList: Array<PostInterface>;
  user: User;

  constructor(private userPostsService: UserPostsService,private router: Router, private userTools: UserTools) { }

  ngOnInit(): void {

    //check if user is logged in, if not redirect them
    if(this.userTools.isUserLoggedIn()){
      this.user = this.userTools.getUser();
      this.userPostsService.getUserPosts(this.userTools.getUserId()).subscribe(response=>{
        this.postList= response;

      },
      error =>{

      });
    }
    else{
      //redirect to previous page or login since user is not logged in
        this.router.navigate(['/login']);

    }

    // reload only once
    if (!localStorage.getItem('marker')) { 
      localStorage.setItem('marker', 'no reload');
      location.reload();
    } else {
      localStorage.removeItem('marker');
    }


  }

}
