import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostEditableComponent } from './post-editable.component';

describe('PostEditableComponent', () => {
  let component: PostEditableComponent;
  let fixture: ComponentFixture<PostEditableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostEditableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostEditableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
