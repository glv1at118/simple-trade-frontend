import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { PostInterface } from '../models/postInterface';
import { User } from '../models/user';
import { PostService } from '../services/post.service';
import { UserTools } from '../UserTools';

@Component({
  selector: 'app-post-editable',
  templateUrl: './post-editable.component.html',
  styleUrls: ['./post-editable.component.css']
})
export class PostEditableComponent implements OnInit {

  @Input() post: PostInterface;
  user: User;
  available: boolean;

  constructor(private userTools: UserTools, private postService: PostService,  private router: Router) { }

  ngOnInit(): void {
    this.available= true;
    this.user = this.userTools.getUser();


  }

  deletePost(){
    this.available= false;
    this.postService.deletePost(this.post.postId).subscribe(response=>{
      this.available= false;

    }, error => {
      this.available= false;
  });
  }

  editPost(){
    //redirect to editpost component, passing postId to it
    this.router.navigate(['/editpost'], {queryParams: {postId: this.post.postId}});

  }

}
