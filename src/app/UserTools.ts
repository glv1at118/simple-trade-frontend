import { User } from "./models/user"


import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class UserTools{

    user: User;

    constructor() { 
        this.loadUser();
    }

    loadUser(){
        try{
            this.user = JSON.parse(localStorage.getItem('user'));
        }
        catch{
            this.user = null;
        }
    }

    isUserLoggedIn(){
        this.loadUser();
        if(this.user==null){
            return false;
        }
        else{
            return true;
        }
    }

    getUser(){
        this.loadUser();
        return this.user;
    }

    getUserId(){
        this.loadUser();
        try{
            return this.user.userId;
        }
        catch{
            return null;
        }
    }

    logOut(){
        localStorage.removeItem('user');
        this.loadUser();
        //need to redirect to homepage here maybe?
    }




}