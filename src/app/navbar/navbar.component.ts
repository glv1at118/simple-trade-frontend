import { Component, DoCheck, EventEmitter, OnInit, Output } from '@angular/core';

import { User } from '../models/user';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, DoCheck {
    isLogged: Boolean = false;
    user: User = null;
    username: string = '';

    constructor() { }

    doLogout(): void {
        localStorage.removeItem('user');
        this.user = null;
        this.isLogged = false;
    }

    ngDoCheck(): void {
        try {
            this.user = JSON.parse(localStorage.getItem('user'));
            this.username = this.user.userName;
            if (this.user != null) {
                this.isLogged = true;
            }
        } catch (error) {
            // console.log('No User Logged in yet')
        }
    }

    ngOnInit(): void {
    }
}