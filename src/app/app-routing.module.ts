import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthComponent } from './auth/auth.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { UpsertComponent } from './upsert/upsert.component';
import { DetailComponent } from './detail/detail.component';
import { UserPostsComponent } from './user-posts/user-posts.component';
import { EditPostComponent } from './edit-post/edit-post.component';

import { ChangePassComponent } from './change-pass/change-pass.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: AuthComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'upsert', component: UpsertComponent },
  { path: 'changePass', component: ChangePassComponent },
  { path: 'detail/:id', component: DetailComponent },
  { path: 'myposts', component: UserPostsComponent},
  { path: 'editpost', component: EditPostComponent},
  { path: '**', redirectTo: 'home'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
