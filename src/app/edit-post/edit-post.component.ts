import { HttpClient, HttpRequest } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostInterface } from '../models/postInterface';
import { User } from '../models/user';
import { PostService } from '../services/post.service';
import { UpsertService } from '../services/upsert.service';
import { UserTools } from '../UserTools';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {

  postId: number;
  user: User;
  post: PostInterface;
  editAccess: boolean;
  fileUrl: string = "";
  fileExist: boolean = false;


  constructor(private route: ActivatedRoute, private postService: PostService, private userTools: UserTools, private postServ: UpsertService, private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params =>{
      this.postId = params['postId'];
    });

    this.user = this.userTools.getUser();
    this.postService.getPost(this.postId).subscribe( res =>{
      this.post = res;
      if(this.post.userHolder.userId == this.user.userId){
        this.editAccess = true;
      }else{
        this.editAccess = false;
      }
      this.fileUrl = this.post.imageURL;

      if (this.fileUrl) {
        this.fileExist = true;
      } else {
        this.fileExist = false;
      }

    });
    
  }

  ngDoCheck(): void {
      if (this.fileUrl) {
          this.fileExist = true;
      } else {
          this.fileExist = false;
    }
  }

    // when a file is chosen, this method is triggered, then it sends the file to spring server.
    chooseImage(event): void {
      let file: File = event.target.files[0];
      let data: FormData = new FormData();
      data.append("file", file);

      let newRequest = new HttpRequest("POST", "http://ec2-54-153-124-53.us-west-1.compute.amazonaws.com:9090/api/upload", data, {
          reportProgress: false,
          responseType: "text"
      });
      this.httpClient.request(newRequest).subscribe(res => {
          this.fileUrl = res["body"];
      }, err => {
      });
  }


  updatePost(){

    
    let stringPost = {
      "postId": this.postId,
      "availability": this.post.availability,
      "postTitle": this.post.postTitle,
      "postDescription": this.post.postDescription,
      "dateCreated": Date.now(),
      "numberOfVisits": this.post.numberOfVisits,
      "price": this.post.price,
      "imageURL": this.fileUrl,
      "userHolder": this.user
    };
    // console.log(stringPost);
    this.postServ.postPost(JSON.stringify(stringPost)).subscribe(
      response=>{
      }
    )
  
  }

}
