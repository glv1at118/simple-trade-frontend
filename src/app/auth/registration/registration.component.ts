import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { RegistrationService } from 'src/app/services/registration.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  userGroup = new FormGroup({
      userName: new FormControl(''),
      email: new FormControl(''),
      passwordHashed: new FormControl(''),
      phoneNumber: new FormControl('')
  });
  constructor(private userServ: RegistrationService, private router: Router) { }

  ngOnInit(): void {
  }

  public submitUser(user: FormGroup){
    let stringUser = JSON.stringify(user.value);
    this.userServ.postUser(stringUser).subscribe(
      response => {
       this.router.navigate(['/login']);
      }
    );
  }
}