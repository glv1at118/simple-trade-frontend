import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  isAuth = false;
  user: User = null;
  message: string = '';

  userGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });

  constructor(private userServ: UserService, private router: Router) { }

  authenticateUser(userGroup: FormGroup): void {
    if (userGroup.get('username').value && userGroup.get('password').value) {
      this.userServ.getAuthUser(userGroup.get('username').value, userGroup.get('password').value)
      .subscribe(
        response => {
          this.user = response;
          if (this.user != null) {
            this.isAuth = true;
            // save user object into localstorage
            localStorage.setItem('user', JSON.stringify(this.user));
            this.router.navigate(['/home']);
          } 
        },
        error => {
          this.message = 'Invalid Credentials';
        }
      );
    }
   
  }

  ngOnInit(): void {
  }

}
