import { Component, DoCheck, OnInit } from '@angular/core';
import { PostInterface } from '../models/postInterface';
import { AllPostsService } from '../services/all-posts.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, DoCheck {

    postList: Array<PostInterface>;
    searchText: string = '';

    constructor(private allPostsService: AllPostsService) { }

    getValue(target: EventTarget): string {
        return (target as HTMLInputElement).value;
    }

    ngDoCheck(): void {
        if (this.postList != undefined || this.postList != null) {
            this.postList.filter(post => {
                const regex = new RegExp(this.searchText, 'gi');
                return post.postTitle.match(regex);
            });
        }
    }

    ngOnInit(): void {
        this.allPostsService.getAllPosts().subscribe(response => {
            this.postList = response;

        },
            error => {

            });
        // reload only once
        if (!localStorage.getItem('marker')) {
            localStorage.setItem('marker', 'no reload');
            location.reload();
        } else {
            localStorage.removeItem('marker');
        }

    }


}
