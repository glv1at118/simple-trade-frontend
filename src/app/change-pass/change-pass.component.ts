import { Component, OnInit } from '@angular/core';
import { RegistrationService } from 'src/app/services/registration.service';
import { Router } from '@angular/router';
import { UserTools } from '../UserTools';

@Component({
  selector: 'app-change-pass',
  templateUrl: './change-pass.component.html',
  styleUrls: ['./change-pass.component.css']
})
export class ChangePassComponent implements OnInit {

  values = '';
constructor(private userServ: RegistrationService, private router: Router, private ut: UserTools) { }

ngOnInit(): void {
}

public submitPass(){
  if(this.values.length>0){
    var user = this.ut.getUser();
    user.passwordHashed = this.values;
    this.userServ.postUser(user).subscribe(
    response => {
      window.alert("Password Updated");
      this.router.navigate(['/home']);
    });
  }
  else window.alert("Password field must not be empty to submit");
}
onKey(event: any) { // without type info
  this.values = event.target.value;
}

}
